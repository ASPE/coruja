<?php
namespace coruja\V1\Rest\Topicos;

class TopicosResourceFactory
{
    public function __invoke($services)
    {
        return new TopicosResource();
    }
}
