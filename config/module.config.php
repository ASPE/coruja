<?php
return array(
    'router' => array(
        'routes' => array(
            'coruja.rest.topicos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/topicos[/:topicos_id]',
                    'defaults' => array(
                        'controller' => 'coruja\\V1\\Rest\\Topicos\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'coruja.rest.topicos',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'coruja\\V1\\Rest\\Topicos\\TopicosResource' => 'coruja\\V1\\Rest\\Topicos\\TopicosResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'coruja\\V1\\Rest\\Topicos\\Controller' => array(
            'listener' => 'coruja\\V1\\Rest\\Topicos\\TopicosResource',
            'route_name' => 'coruja.rest.topicos',
            'route_identifier_name' => 'topicos_id',
            'collection_name' => 'topicos',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'coruja\\V1\\Rest\\Topicos\\TopicosEntity',
            'collection_class' => 'coruja\\V1\\Rest\\Topicos\\TopicosCollection',
            'service_name' => 'topicos',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'coruja\\V1\\Rest\\Topicos\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'coruja\\V1\\Rest\\Topicos\\Controller' => array(
                0 => 'application/vnd.coruja.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'coruja\\V1\\Rest\\Topicos\\Controller' => array(
                0 => 'application/vnd.coruja.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'coruja\\V1\\Rest\\Topicos\\TopicosEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'coruja.rest.topicos',
                'route_identifier_name' => 'topicos_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'coruja\\V1\\Rest\\Topicos\\TopicosCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'coruja.rest.topicos',
                'route_identifier_name' => 'topicos_id',
                'is_collection' => true,
            ),
        ),
    ),
    'zf-content-validation' => array(
        'coruja\\V1\\Rest\\Topicos\\Controller' => array(
            'input_filter' => 'coruja\\V1\\Rest\\Topicos\\Validator',
        ),
    ),
    'input_filter_specs' => array(
        'coruja\\V1\\Rest\\Topicos\\Validator' => array(
            0 => array(
                'required' => true,
                'validators' => array(),
                'filters' => array(),
                'name' => 'nome',
                'description' => 'Nome do tópico',
            ),
        ),
    ),
);
